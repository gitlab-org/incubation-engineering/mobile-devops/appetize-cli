# frozen_string_literal: true

require_relative "../command"
require_relative "../api"

module Appetize
  module Commands
    class Update < Appetize::Command
      def initialize(path, public_key, token, api_host, options)
        @api        = Appetize::API.new(token, api_host)
        @path       = path
        @public_key = public_key
        @options    = options
      end

      def execute(input: $stdin, output: $stdout)
        response = @api.update(@path, @public_key)

        raise "Update failed: #{response.code}" unless response.code == 200

        output.puts response.body
      end
    end
  end
end
