# frozen_string_literal: true

require_relative "../command"
require_relative "../api"

module Appetize
  module Commands
    class Delete < Appetize::Command
      def initialize(public_key, token = nil, api_host = nil, options = {})
        @api        = Appetize::API.new(token, api_host)
        @public_key = public_key
        @options    = options
      end

      def execute(input: $stdin, output: $stdout)
        response = @api.delete(@public_key)

        raise "Delete failed: #{response.code}" unless response.code == 200

        output.puts response.body
      end
    end
  end
end
