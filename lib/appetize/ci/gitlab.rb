# frozen_string_literal: true

require "httparty"

module Appetize
  module CI
    class GitLab
      def initialize(project_id, ref_name, api_token, api_host = nil)
        @project_id   = project_id
        @ref_name     = ref_name
        @api_token    = api_token
        @api_host     = api_host || "https://gitlab.com/api/v4"
      end

      def fetch_artifact(job, artifact_name)
        url = "#{@api_host}/projects/#{@project_id}/jobs/artifacts/#{@ref_name}/raw/#{artifact_name}?job=#{job}"

        headers = {
          "PRIVATE-TOKEN" => @api_token
        }

        HTTParty.get(url, headers: headers)
      end
    end
  end
end
