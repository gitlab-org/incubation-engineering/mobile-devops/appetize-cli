# frozen_string_literal: true

require "httparty"

module Appetize
  class API
    def initialize(token = nil, api_host = nil)
      @token    = token || ENV["APPETIZE_API_TOKEN"]
      @api_host = api_host || "api.appetize.io"

      raise "Appetize Token Missing" if @token.nil?
    end

    def create(path, platform)
      url = "https://#{@api_host}/v1/apps"
      auth = { username: @token, password: "" }
      body = { platform: platform, file: File.open(path) }

      HTTParty.post(url, body: body, basic_auth: auth)
    end

    def update(path, public_key)
      url = "https://#{@api_host}/v1/apps/#{public_key}"
      auth = { username: @token, password: "" }
      body = { file: File.open(path) }

      HTTParty.post(url, body: body, basic_auth: auth)
    end

    def delete(public_key)
      url = "https://#{@api_host}/v1/apps/#{public_key}"
      auth = { username: @token, password: "" }

      HTTParty.delete(url, basic_auth: auth)
    end
  end
end
