# frozen_string_literal: true

require_relative "appetize/version"

module Appetize
  class Error < StandardError; end
end
