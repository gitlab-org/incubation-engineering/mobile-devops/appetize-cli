# frozen_string_literal: true

RSpec.describe Appetize do
  it "has a version number" do
    expect(Appetize::VERSION).not_to be nil
  end
end
