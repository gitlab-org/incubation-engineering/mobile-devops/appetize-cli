# frozen_string_literal: true

require "appetize/commands/update"

RSpec.describe Appetize::Commands::Update do
  it "executes `update` command successfully" do
    response = { publicKey: "abc123" }
    expect(File).to receive(:open)
    expect(HTTParty).to receive(:post).and_return(OpenStruct.new(code: 200, body: response.to_json))

    output = StringIO.new
    command = Appetize::Commands::Update.new("foo/bar", "abc123", "fake_token", nil, {})

    command.execute(output: output)

    expect(output.string).to eq("#{response.to_json}\n")
  end

  it "returns an error if the `update` command fails with a 500" do
    expect(File).to receive(:open)
    expect(HTTParty).to receive(:post).and_return(Net::HTTPServerError.new("1.1", 500, ""))

    output = StringIO.new
    command = Appetize::Commands::Update.new("foo/bar", "abc123", "fake_token", nil, {})

    expect { command.execute(output: output) }.to raise_error(RuntimeError, "Update failed: 500")
  end

  it "returns an error if the `update` command fails with a 401" do
    expect(File).to receive(:open)
    expect(HTTParty).to receive(:post).and_return(Net::HTTPUnauthorized.new("1.1", 401, ""))

    output = StringIO.new
    command = Appetize::Commands::Update.new("foo/bar", "abc123", "fake_token", nil, {})

    expect { command.execute(output: output) }.to raise_error(RuntimeError, "Update failed: 401")
  end

  it "returns an error if the `update` command fails with a 404" do
    expect(File).to receive(:open)
    expect(HTTParty).to receive(:post).and_return(Net::HTTPUnauthorized.new("1.1", 404, ""))

    output = StringIO.new
    command = Appetize::Commands::Update.new("foo/bar", "abc123", "fake_token", nil, {})

    expect { command.execute(output: output) }.to raise_error(RuntimeError, "Update failed: 404")
  end
end
