# frozen_string_literal: true

require "appetize/api"

RSpec.describe Appetize::API do
  describe "@token" do
    it "assigns APPETIZE_API_TOKEN when no value supplied" do
      stub_const("ENV", { "APPETIZE_API_TOKEN" => "fake_env" })
      api = Appetize::API.new
      expect(api.instance_variable_get(:@token)).to eq("fake_env")
    end

    it "assigns the supplied value when provided" do
      stub_const("ENV", { "APPETIZE_API_TOKEN" => "fake_env" })
      api = Appetize::API.new("fake_token")
      expect(api.instance_variable_get(:@token)).to eq("fake_token")
    end

    it "raises when no value can be set" do
      expect { Appetize::API.new }.to raise_error("Appetize Token Missing")
    end
  end

  describe "@api_host" do
    before do
      stub_const("ENV", { "APPETIZE_API_TOKEN" => "fake_env" })
    end

    it "assigns the supplied value when provided" do
      api = Appetize::API.new(nil, "fake.host.com")
      expect(api.instance_variable_get(:@api_host)).to eq("fake.host.com")
    end

    it "defaults to api.appetize.io when no value supplied" do
      api = Appetize::API.new
      expect(api.instance_variable_get(:@api_host)).to eq("api.appetize.io")
    end
  end

  describe "#create" do
    it "executes the expected http request" do
      allow(File).to receive(:open)
      expect(HTTParty).to receive(:post).with(
        "https://api.appetize.io/v1/apps",
        body: { platform: "android", file: File.open("fake/path.apk") },
        basic_auth: { username: "fake_token", password: "" }
      )

      api = Appetize::API.new("fake_token")
      api.create("fake/path.apk", "android")
    end
  end

  describe "#update" do
    it "executes the expected http request" do
      allow(File).to receive(:open)
      expect(HTTParty).to receive(:post).with(
        "https://api.appetize.io/v1/apps/fake_public_key",
        body: { file: File.open("fake/path.apk") },
        basic_auth: { username: "fake_token", password: "" }
      )

      api = Appetize::API.new("fake_token")
      api.update("fake/path.apk", "fake_public_key")
    end
  end

  describe "#delete" do
    it "executes the expected http request" do
      expect(HTTParty).to receive(:delete).with(
        "https://api.appetize.io/v1/apps/fake_public_key",
        basic_auth: { username: "fake_token", password: "" }
      )

      api = Appetize::API.new("fake_token")
      api.delete("fake_public_key")
    end
  end
end
