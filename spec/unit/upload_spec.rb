# frozen_string_literal: true

require "appetize/commands/upload"

RSpec.describe Appetize::Commands::Upload do
  it "executes `upload` command successfully" do
    response = { publicKey: "abc123" }
    expect(File).to receive(:open)
    expect(HTTParty).to receive(:post).and_return(OpenStruct.new(code: 200, body: response.to_json))

    output = StringIO.new
    command = Appetize::Commands::Upload.new("foo/bar", "ios", "fake_token", nil, {})

    command.execute(output: output)

    expect(output.string).to eq("#{response.to_json}\n")
  end

  it "returns an error if the `upload` command fails with a 500" do
    expect(File).to receive(:open)
    expect(HTTParty).to receive(:post).and_return(Net::HTTPServerError.new("1.1", 500, ""))

    output = StringIO.new
    command = Appetize::Commands::Upload.new("foo/bar", "ios", "fake_token", nil, {})

    expect { command.execute(output: output) }.to raise_error(RuntimeError, "Upload failed: 500")
  end

  it "returns an error if the `upload` command fails with a 401" do
    expect(File).to receive(:open)
    expect(HTTParty).to receive(:post).and_return(Net::HTTPUnauthorized.new("1.1", 401, ""))

    output = StringIO.new
    command = Appetize::Commands::Upload.new("foo/bar", "ios", "fake_token", nil, {})

    expect { command.execute(output: output) }.to raise_error(RuntimeError, "Upload failed: 401")
  end

  describe ".gitlab_ci?" do
    it "returns true if GITLAB_CI is the string true" do
      stub_const("ENV", { "GITLAB_CI" => "true" })
      expect(Appetize::Commands::Upload.gitlab_ci?).to be true
    end

    it "returns true if GITLAB_CI is true" do
      stub_const("ENV", { "GITLAB_CI" => true })
      expect(Appetize::Commands::Upload.gitlab_ci?).to be true
    end

    it "returns false if GITLAB_CI is not the string true" do
      stub_const("ENV", { "GITLAB_CI" => "foo" })
      expect(Appetize::Commands::Upload.gitlab_ci?).to be false
    end

    it "returns false if GITLAB_CI is false" do
      stub_const("ENV", { "GITLAB_CI" => false })
      expect(Appetize::Commands::Upload.gitlab_ci?).to be false
    end

    it "returns false if GITLAB_CI is not set" do
      stub_const("ENV", { "GITLAB_CI" => nil })
      expect(Appetize::Commands::Upload.gitlab_ci?).to be false
    end
  end

  it "checks the GitLab API for artifacts if running in GitLab CI" do
    response = { publicKey: "abc123" }
    stub_const("ENV", {
                 "GITLAB_CI" => true,
                 "CI_PROJECT_ID" => 12_345,
                 "CI_COMMIT_REF_NAME" => "some/branch/name",
                 "PRIVATE_TOKEN" => "someprivatetoken"
               })

    expect(File).to receive(:open)
    expect(HTTParty).to receive(:get)
      .with(
        "https://gitlab.com/api/v4/projects/12345/jobs/artifacts/some/branch/name/raw/appetize-information.json?job=startReview",
        headers: { "PRIVATE-TOKEN" => "someprivatetoken" }
      )
      .and_return(OpenStruct.new(code: 200, body: { publicKey: "abc123" }.to_json))
    expect(HTTParty).to receive(:post).and_return(OpenStruct.new(code: 200, body: response.to_json))

    output = StringIO.new
    command = Appetize::Commands::Upload.new("foo/bar", "ios", "fake_token", nil, {})

    command.execute(output: output)

    expect(output.string).to eq("#{response.to_json}\n")
  end
end
